from urllib.robotparser import RequestRate
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import TemplateView, RedirectView, View

from .models import User
from .forms import UserForm

# Create your views here.

class AddData(TemplateView):
    template_name = 'core/add_del.html'

    def get(self, request):
        form = UserForm()
        user = User.objects.all()
        context = {
            'form': form,
            'users': user
        }

        return render(request, 'core/add_del.html', context)

    def post(self, request):
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
        form = UserForm()
        user = User.objects.all()
        context = {
            'form': form,
            'users': user
        }
        return render(request, 'core/add_del.html', context)


class DeleteData(View):
    def post(self, request, id):
        User.objects.get(id=id).delete()
        return HttpResponseRedirect('/')

class UpdateData(View):
    def get(self, request, id,  *args, **kwargs):
        user = User.objects.get(id=id)
        form = UserForm(instance=user)
        context = {
            'form': form
        }
        return render(request, 'core/update.html', context)

    def post(self, request, id, *args, **kwargs):
        user = User.objects.get(id=id)
        form = UserForm(instance=user, data=request.POST)
        if form.is_valid():
            form.save()
        context = {
            'form': form
        }
        return HttpResponseRedirect('/')
