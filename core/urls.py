from django.urls import path

from .views import *


urlpatterns = [ 
    path('', AddData.as_view(), name='add'),
    path('delete/<int:id>/', DeleteData.as_view(), name='del'),
    path('update/<int:id>/', UpdateData.as_view(), name='update'),
]